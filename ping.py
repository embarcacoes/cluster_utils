#!/usr/bin/python
import os

def ping(hostname: str, num: int = 1) -> bool:
    return not os.system(f"ping -c {num} -q {hostname}")

hosts = [
    "192.168.4.3",
    "192.168.4.4",
    "192.168.4.5",
    "192.168.4.6",
    "192.168.4.7",
    "192.168.4.8",
    "192.168.4.9",
    "192.168.4.10",
]

print("APP iniciando testes")

for i, v in enumerate(hosts):
    print(f"Estado NO {i+1} - {'online' if ping(v) else 'desconectado'}",end="\n\n")